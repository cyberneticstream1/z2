import { initializeApp } from "firebase/app";
import {getFirestore, collection, query, where, getDocs} from "firebase/firestore"

const firebaseConfig = {
    apiKey: "AIzaSyDCR8eInTmio5pBhmrp1EwPrO2MpWm0e5M",
    authDomain: "cybernetic-stream-37edf.firebaseapp.com",
    projectId: "cybernetic-stream-37edf",
    storageBucket: "cybernetic-stream-37edf.appspot.com",
    messagingSenderId: "582106675005",
    appId: "1:582106675005:web:3fe94fb7e071242fe83754"
};

const app = initializeApp(firebaseConfig);
export const db = getFirestore()
